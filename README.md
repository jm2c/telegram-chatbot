# Telegram chatbot
## Careaga Carrillo Juan Manuel

Nombre del chatbot: @jmcareagabot (juan_bot)

El funcionemiento es muy sencillo, hay dos instancias EC2 de AWS cada
una con su propia ip elástica.

La primera es un servidor web en el cual se ejecuta principalmente
Apache2 con PHP, este servidor tiene un dominio asociado el cual es
[https://juancarea.ga](https://juancarea.ga). Al servidor Apache se le
realizó una configuración proxy para servir una aplicación NODE
(server.js) en la ruta /srever que es una aplicación hecha en
express.js que toma algunos datos del servidor (uso del cpu, uso de
memoria, velocidad de conexión) y los regresa como una cadena JSON.

La segunda instancia no tiene asociada un dominio, pues no es
necesario. Aquí solamente está corriendo un script también en NODE
(bot.js) que es quien se conecta con la API de Telegram. Este script
recibe los comandos del usuario, se comunica a traves de https al
primer servidor para obtener el JSON con lainformación del servidor y
manda esa información de regreso a telegram.

![Diagrama](https://gitlab.com/jm2c/telegram-chatbot/-/raw/master/Telegram%20Chatbot.png "Diagrama del proyecto")