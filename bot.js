const { Telegraf } = require('telegraf')
const bot = new Telegraf('1670117724:AAEbxz6jvCy94lk6Yoq7R8RKaj4numlHs7k')

const https = require('https')
const url = 'https://juancarea.ga/server'


bot.start(ctx => {
    console.log(ctx)
    ctx.reply('Hola Mundo!!!')
    ctx.reply('Para obtener la información del servidor utiliza el comando /getData')
})

bot.command('getData', ctx => {
    let obj = ''
    https.get(url, res => {
	console.log('status:', res.statusCode)
	console.log('headers:', res.headers)
	
	res.on('data', d => {
	    obj += d
	})
	res.on('end', () => {
	    obj = JSON.parse(obj)
	    console.log(obj, typeof obj)
	    ctx.reply(`Uso del cpu: ${obj.cpu}%\nMemoria en uso: ${obj.mem}%\nVelocidad de conexión: ${obj.speed}`)
	})
    }).on('error', e => console.error(e.message))
})

bot.launch()

