const si = require('systeminformation')
const osu = require('node-os-utils')
const cpu = osu.cpu
const mem = osu.mem

const express = require('express')
const app = express()
const PORT = 8000

app.get('/', (req,res) => {
    let data = {}
    cpu.usage()
	.then(cpuPerc => {
	    data.cpu = cpuPerc
	})
	.then(() => {
	    mem.info()
		.then(info => {
		    data.mem = 100 - info.freeMemPercentage
		}).then(() => {
		    si.networkInterfaces()
			.then(stat => {
			    data.speed = stat[0].speed
			})
		    	.then(() => {
			    res.send( JSON.stringify(data)  )
			})
		})
	})
})

app.listen(PORT, () => {
    console.log(`Listening port ${PORT}`)
})
